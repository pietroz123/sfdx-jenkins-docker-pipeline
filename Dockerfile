# node > 14.6.0 is required for the SFDX-Git-Delta plugin
FROM node:14.14-alpine

#add usefull tools
RUN apk add --update --no-cache  \
  git \
  findutils \
  bash \
  unzip \
  curl \
  wget \
  nodejs-npm \
  openjdk8-jre \
  openssh-client \
  perl \
  jq

# install Salesforce CLI from npm
RUN npm install sfdx-cli --global
RUN sfdx --version

# install sfpowerkit - https://github.com/Accenture/sfpowerkit
RUN mkdir .local && chmod g+rwx .local \
  && echo 'y' | sfdx plugins:install sfpowerkit
RUN sfdx plugins
